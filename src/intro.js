import React, { Component } from 'react';
import './App.css';

class Intro extends Component {
  render() {
    return (
      <div className="Intro">
        <header className="header">
          <a
            className="link"
            href="https://reactjs.org"
          >
            Ingresar
          </a>
        </header>
      </div>
    );
  }
}

export default Intro;
